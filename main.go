package main

import (
	"fmt"
	"net/http"

	"gitlab.com/gitops-demo/apps/my-go-app5/v2/hello"
	"gitlab.com/gitops-demo/apps/my-go-app5/v2/parser"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		errorHandler(w, r, http.StatusNotFound)
		return
	})
	http.HandleFunc("/hello", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "A Wonderful Proverb: %s\n", hello.Proverb())
	})
	parser.ParseComplex([]byte("F"))
	http.ListenAndServe(":3000", nil)
}

func errorHandler(w http.ResponseWriter, r *http.Request, status int) {
	w.WriteHeader(status)
	if status == http.StatusNotFound {
		fmt.Fprint(w, "404!\nHello, you've requested: ", r.URL.Path)
	}
}
